FROM python:3.8-alpine

COPY beyondvcr/ beyondvcr/

EXPOSE 80

CMD ["python", "beyondvcr/server.py", "--port", "80"]
