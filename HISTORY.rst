=======
History
=======

0.1.0a1 (2020-06-09)
####################

* First release on PyPI.


0.1.1 (2022-06-13)
##################

* Added basic documentation.
* Added linters, and improved maintenance tooling.
