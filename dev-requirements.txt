flake8
coverage
twine
wheel
black
mypy

# typing stubs:
types-setuptools
types-requests
